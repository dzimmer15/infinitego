
## infiniteGO48



This game project is based on a twitter post I had lost. It plays like an  
amalgamation of GO and 2048. There is a 4x4 board that has black or white GO
pieces randomly spawned after each interaction. Interactions are just like 
2048, (up, down, left, and right). After each interaction the game pushed all 
of the pieces to one side of the board and then the game checks if any
white or black pieces completly surround a group of the opposite color, if so
then the surrounded pieces are removed from play. The game continues until the
board is completely filed with pieces that can't make groups. 

This game is far superior to 2048 because the sense of progression is entirely
based on how well you can manage pieces instead of just keeping them all to one 
side.