import pygame
import numpy
import random
import time
import sys
from enum import Enum


pygame.init()

Black = pygame.Color(0,0,0,255)
White = pygame.Color(255,255,255)

play_grid_width = 4
size = window_width, window_height = 640, 480
screen = pygame.display.set_mode(size)





class Player(Enum):
	
	White = 1
	Black = 2

class Direction(Enum):
	Left = 1
	Right = 2
	Up = 3
	Down = 4


class PieceFactory:
	
	def getPiece( posX, posY, player):
		if (player == Player.Black):
			color = Black
		elif (player == Player.White):
			color = White
		else:
			print("Piece has no color")


		return Piece(Player,color, posX, posY)



class Piece:
	player = None
	gridX, gridY = 0, 0
	posX, posY = 0, 0
	color = None
	radius = 43

	def __init__(self, player, color, gridX, gridY, posX = 0, posY = 0):
		self.player = player
		self.gridX = gridX
		self.gridY = gridY
		self.posX = posX
		self.posY = posY
		self.color = color

	def draw(self, screen):
		pygame.draw.circle(screen, self.color, (self.posX, self.posY), self.radius)

		
		


class Grid:
	width, height = 0,0
	cells = 0
	pieces = None


	def __init__(self, width, height = None):

		self.width = width
		if height == None:
			self.height = width
		else:
			self.height = height
		self.cells = self.height * self.width
		self.pieces = [[None for x in range(width)] for y in range(width)]

		

class Board:
	
	width, height = 0,0
	X,Y = 0,0
	grid_square_width, grid_square_height = 0,0

	grid = None
	piece_factory = None

	def __init__(self, width, height = None, grid = 4):
		self.width = width
		if height == None:
			self.height = width
		else:
			self.height = height
		self.X, self.Y = (window_width - self.width) // 2 , (window_height - self.height) // 2



		self.grid = Grid(grid)
		self.piece_factory = PieceFactory

		self.grid_square_width, self.grid_square_height = self.width / self.grid.width , self.height / self.grid.height


	def draw(self, screen):
		for x in range(self.grid.width):
			for y in range(self.grid.height):
				if (self.grid.pieces[x][y] != None):
					self.grid.pieces[x][y].draw(screen)
			
		

	def addPiece(self, posX, posY, player):
		newPiece = self.piece_factory.getPiece(posX, posY, player)

		
		newPiece.posX = self.X + int((posX * self.grid_square_width) + self.grid_square_width//2)
		newPiece.posY = self.Y + int((posY * self.grid_square_height) + self.grid_square_height//2)
		
		self.grid.pieces[posX][posY] = newPiece


	
	def shiftBoard(self, direction):
		print("not implemented")


board = Board(400,400, 4)



def draw(screen):
	global board
	screen.fill(pygame.Color(20,60,140))
	pygame.draw.rect(screen, pygame.Color(60,40,10), pygame.Rect(board.X,board.Y,board.width,board.height))

	board.draw(screen)
				#print(pos, pos_color)

	pygame.display.flip()




def init():
	frame_check = time.time() + 1


def run():
	global frames, frame_check, frame_length, pieceFactory

	prev_frame = cur_frame	
	cur_frame = time.time()

	for event in pygame.event.get():
		if event.type == pygame.QUIT: sys.exit()
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_SPACE:
				print("SPACE key pressed")
				randX, randY = random.randint(0, 3), random.randint(0,3)
				print("Making new piece at" , randX , randY)

				collision_test = True
				while (collision_test):
					if (ticks % 2 > 0):
						board.addPiece(randX,randY, Player.White)
					else:
						board.addPiece(randX,randY, Player.Black)




			if event.key == pygame.K_w or event.key == pygame.K_UP:
				print("up")
				board.shiftBoard(Direction.Up)
			if event.key == pygame.K_s or event.key == pygame.K_DOWN:
				board.shiftBoard(Direction.Down)
				print("down")
			if event.key == pygame.K_a or event.key == pygame.K_LEFT:
				board.shiftBoard(Direction.Left)
				print("left")
			if event.key == pygame.K_d or event.key == pygame.K_RIGHT:
				board.shiftBoard(Direction.Right)
				print("right")



	fps_limit = 60
	frame_length = 1/(fps_limit + 1)
	frames = 0
	frame_check = 0
	tick_count = 0





	end_time = time.time()
	tick_count += 1




### frame code should be substituted for ticks

	# if frame_check == 0:
	# 	frame_check = time.time() + 1
	# curtime = time.time()
	
	# draw(screen)

	# newtime = time.time()
	# delta = newtime - curtime

	# sleep_time = frame_length - delta 
	# if sleep_time > 0:
	# 	time.sleep(frame_length - delta )
	# newtime = time.time()

	# if(newtime <= frame_check):
	# 	frames += 1
	# else:
	# 	frame_check = time.time() + 1
	# 	print(frames)
	# 	frames = 0




	

	
# Entry point

init()

while True:
	run()


